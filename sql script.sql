create DATABASE [projectDev]
GO
/****** Object:  Table [dbo].[DocumentMarkers]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentMarkers](
	[DocID] [uniqueidentifier] NULL,
	[MarkerID] [uniqueidentifier] NOT NULL,
	[MarkerType] [varchar](25) NOT NULL,
	[MarkerLocationX] [int] NOT NULL,
	[MarkerLocationY] [int] NOT NULL,
	[ForeColor] [varchar](25) NOT NULL,
	[BackColor] [varchar](25) NOT NULL,
	[UserID] [varchar](25) NOT NULL,
	[RadiusY] [int] NULL,
	[RadiusX] [int] NULL,
	[myText] [varchar](8000) NULL,
	[dateCreate] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[MarkerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Documents]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Documents](
	[UserID] [varchar](25) NOT NULL,
	[DocumentName] [varchar](25) NOT NULL,
	[DocID] [uniqueidentifier] NOT NULL,
	[ImageURL] [varchar](max) NULL,
	[dateCreate] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[DocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SharedDocuments]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SharedDocuments](
	[DocID] [uniqueidentifier] NOT NULL,
	[UserID] [varchar](25) NOT NULL,
	[statuse] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[DocID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserID] [varchar](25) NOT NULL,
	[UserName] [varchar](25) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DocumentMarkers] ADD  DEFAULT (newid()) FOR [MarkerID]
GO
ALTER TABLE [dbo].[Documents] ADD  DEFAULT (newid()) FOR [DocID]
GO
ALTER TABLE [dbo].[DocumentMarkers]  WITH CHECK ADD FOREIGN KEY([DocID])
REFERENCES [dbo].[Documents] ([DocID])
GO
ALTER TABLE [dbo].[DocumentMarkers]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[Documents]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
ALTER TABLE [dbo].[SharedDocuments]  WITH CHECK ADD FOREIGN KEY([DocID])
REFERENCES [dbo].[Documents] ([DocID])
GO
ALTER TABLE [dbo].[SharedDocuments]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[Users] ([UserID])
GO
/****** Object:  StoredProcedure [dbo].[changeMarker]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[changeMarker] @markerID uniqueIdentifier,@docID  uniqueIdentifier,@MarkerType varchar(25),@markerLocationX int,@MarkerLocationY int,
@radiusX int,@radiusY int,@foreColor varchar(25),@backColor varchar(25),@userID varchar(25),@myText varchar(8000)
AS
BEGIN
update [dbo].[DocumentMarkers]
set MarkerType=@MarkerType,markerLocationX=@markerLocationX,markerLocationY=@markerLocationY,
 radiusX=@radiusX,radiusY=@radiusY,foreColor=@foreColor,backColor=@backColor,userID=@userID,myText=@myText
where markerID=@markerID
end
GO
/****** Object:  StoredProcedure [dbo].[CreateDocument]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[CreateDocument] @userID varchar(25),@ImageURL varchar(max) ,@DocumentName varchar(25)
AS
BEGIN
declare @anser int
select @anser=count(*)
from [dbo].[Users]
where userid=@userID
if(@anser>0)
begin
insert into Documents(userID,ImageURL,DocumentName,dateCreate) 
output inserted.userID,inserted.DocumentName,inserted.DocID,inserted.ImageURL,convert(varchar,inserted.dateCreate) dataString,1 as statuse
values (@userID,@ImageURL,@DocumentName,getDate())
end
END
GO
/****** Object:  StoredProcedure [dbo].[CreateMarker]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE PROCEDURE [dbo].[CreateMarker] @DocID uniqueIdentifier ,@MarkerType varchar(25),@markerLocationX int,@MarkerLocationY int,@radiusX int,@radiusY int,@foreColor varchar(25),@backColor varchar(25),@userID varchar(25),@myText varchar(8000)
AS
BEGIN
declare @c1 int , @c2 int
select  @c1=count(*)
from [dbo].[Users]
where userId=@userID
select  @c2=count(*)
from [dbo].[Documents]
where DocID=@DocID
if(@c1>0 and @c2>0)
begin
insert into [dbo].[DocumentMarkers](DocID,MarkerType,MarkerLocationX,MarkerLocationY,ForeColor,BackColor,UserID,RadiusX,RadiusY,myText,dateCreate) output inserted.*
values (@DocID,@MarkerType,@MarkerLocationX,@MarkerLocationY ,@foreColor,@backColor,@userID,@radiusX,@radiusY,@myText,GETDATE())
end
END
GO
/****** Object:  StoredProcedure [dbo].[CreateShare]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--USE [projectDev++]
--GO

--/****** Object:  StoredProcedure [dbo].[CreateShare]    Script Date: 12/10/2020 11:13:15 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO




--ALTER PROCEDURE [dbo].[CreateShare] @DocID uniqueIdentifier,@UserID varchar(25)
--AS
--BEGIN
--declare @c1 int , @c2 int
--select  @c1=count(*)
--from [dbo].[Users]
--where userId=@userID
--select  @c2=count(*)
--from [dbo].[Documents]
--where DocID=@DocID
--if(@c1>0 and @c2>0)
--begin
--insert into	SharedDocuments(DocID,UserID) 
--values (@DocID,@UserID)
--return 1
--end
--return 0
--END
--GO






CREATE PROCEDURE [dbo].[CreateShare] @DocID uniqueIdentifier,@UserID varchar(25),@statuse int
AS
BEGIN
declare @c1 int , @c2 int,@c3 int,@userDoc varchar(25)
select  @c1=count(*)
from [dbo].[Users]
where userId=@userID
select  @c2=count(*)
from [dbo].[Documents]
where DocID=@DocID
select  @userDoc=[UserID]
from [dbo].[Documents]
where DocID=@DocID
select  @c3=count(*)
from [dbo].[SharedDocuments]
where [UserID]=@userDoc and [DocID]=@DocID
if(@c1>0 and @c2>0 and @c3<1)
begin
insert into SharedDocuments(DocID,UserID,statuse)
values (@DocID,@userDoc,1)
end
select @c3=count(*)
from [dbo].[SharedDocuments]
where UserID=@UserID and DocID=@DocID
if(@c1>0 and @c2>0 and @c3<1)
begin
insert into SharedDocuments(DocID,UserID,statuse)
values (@DocID,@UserID,@statuse)
if(@c1>0 and @c2>0 and @c3>0)
begin
update SharedDocuments
set statuse=@statuse
where docID=@DocID and userID=@UserID
end
return 1
end
return 0
END
GO
/****** Object:  StoredProcedure [dbo].[CreateUser]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateUser] @UserID varchar(25),@UserName varchar(25)
AS
BEGIN
	Insert Into Users(UserID,UserName)
	values (@UserID,@UserName)
END
GO
/****** Object:  StoredProcedure [dbo].[GetDocument]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetDocument] @DocID uniqueIdentifier
AS
BEGIN
select userID,DocumentName,DocID,ImageURL
,convert(varchar,dateCreate) dataString,1 statuse
from Documents
where DocID = @DocID
END
GO
/****** Object:  StoredProcedure [dbo].[GetDocuments]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDocuments] 
AS
BEGIN
select *
from Documents
order by dateCreate
END
GO
/****** Object:  StoredProcedure [dbo].[GetDocumentsForUser]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






--ALTER PROCEDURE [dbo].[GetDocumentsForUser] @userID varchar(25)
--as
--begin
--select distinct *
--from [dbo].[Documents] d join [dbo].[SharedDocuments] s
--on d.DocID=s.DocID
--where d.userID=@userID or s.UserID=@userID
--end
--GO
CREATE PROCEDURE [dbo].[GetDocumentsForUser] @userID varchar(25)
as
begin
select distinct  [Documents].dateCreate,[Documents].userID,[Documents].DocumentName,[Documents].DocID,[Documents].ImageURL
,convert(varchar,[Documents].dateCreate) dataString,COALESCE(null,1) statuse
from [dbo].[Documents]
where DocID in(select DocID from [Documents] where userID=@userID) 
UNION
select distinct  doc.dateCreate,doc.userID,doc.DocumentName,doc.DocID,doc.ImageURL
,convert(varchar,doc.dateCreate) dataString,share.statuse 
from Documents doc join SharedDocuments share
on doc.DocID=share.DocID
where share.Userid =@userID
order by dateCreate
end
GO
/****** Object:  StoredProcedure [dbo].[GetMarkers]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetMarkers] @DocID uniqueIdentifier
AS
BEGIN
select *
from DocumentMarkers
where  DocID=@DocID
order by dateCreate
END
GO
/****** Object:  StoredProcedure [dbo].[GetShare]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetShare] @DocID uniqueIdentifier,@userID varchar(25),@statuse int
AS
BEGIN
select DocID,UserID,statuse
from [dbo].[SharedDocuments]
where DocID=@DocID and userID=@userID
END
GO
/****** Object:  StoredProcedure [dbo].[GetShares]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[GetShares] @docID uniqueIdentifier,@userID varchar(25)
AS
BEGIN
select *
from [dbo].[SharedDocuments]
where @docID=DocID
end
GO
/****** Object:  StoredProcedure [dbo].[GetUser]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUser] @userID varchar(25)
AS
BEGIN
select *
from [dbo].[Users]
where userID=@userID
END
GO
/****** Object:  StoredProcedure [dbo].[GetUsers]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUsers] 
AS
BEGIN
select *
from [dbo].[Users]
END
GO
/****** Object:  StoredProcedure [dbo].[GetUsersOfShare]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetUsersOfShare] @docID uniqueIdentifier

AS
BEGIN
	select UserID
    from [dbo].[SharedDocuments]
    where DocID=@docID
END
GO
/****** Object:  StoredProcedure [dbo].[Login]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[Login] @UserID varchar(25),@UserName varchar(25)
AS
BEGIN
select count(*)
from Users
where UserID=@UserID and UserName=@UserName
END
GO
/****** Object:  StoredProcedure [dbo].[MarkUserRemoved]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[MarkUserRemoved] @UserID varchar(25)
AS
BEGIN
delete from [dbo].[DocumentMarkers] 
where UserID=@UserID
delete from [dbo].[Documents]
where UserID=@UserID
delete from [dbo].[SharedDocuments]
where UserID=@UserID
    delete from Users
	where UserID=@UserID
END
GO
/****** Object:  StoredProcedure [dbo].[RemoveDocument]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RemoveDocument] @DocID uniqueIdentifier 
AS
BEGIN
delete
from [dbo].[SharedDocuments]
where DocID=@DocID
delete
from [dbo].[DocumentMarkers]
where DocID=@DocID
delete
from Documents
where DocID = @DocID 
END
GO
/****** Object:  StoredProcedure [dbo].[RemoveMarker]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[RemoveMarker] @DocMarkerID uniqueIdentifier
AS
BEGIN
delete
from DocumentMarkers
where MarkerID=@DocMarkerID
END
GO
/****** Object:  StoredProcedure [dbo].[RemoveShare]    Script Date: 16/11/2020 11:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[RemoveShare] @DocID varchar(25),@UserID varchar(25)
AS
BEGIN
delete 
from [dbo].[SharedDocuments]
where DocID=CONVERT(uniqueIdentifier,@DocID) and UserID=@UserID
END
GO
